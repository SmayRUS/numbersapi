import controller.NumberApiController;

import java.io.IOException;

public class Application {

    public static void main(String[] args) {
        NumberApiController controller = new NumberApiController();

        try {
            controller.run(args);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
