package configuration;

import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

public class PropertiesConfiguration {
    public Map<String, String> defineProperties() {
        Properties properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream("application.properties"));

        } catch (Exception e) {
            e.getMessage();
        }

        Map<String, String> propertiesValue = new TreeMap<>();
        properties.keySet().forEach((k) -> propertiesValue.put(k.toString(), properties.get(k).toString()));

        return  propertiesValue;
    }
}
