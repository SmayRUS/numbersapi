package configuration;

import annotation.Value;

import java.lang.reflect.Field;

public class ValueInjector {

    public static void inject(Object instance) {
        Field[] fields = instance.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(Value.class)) {
                Value value = field.getAnnotation(Value.class);
                field.setAccessible(true);
                try {
                    field.set(instance, takePropertieValue(value.value()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                field.setAccessible(false);
            }
        }
    }

    private static String takePropertieValue(String value) {
        return new PropertiesConfiguration().defineProperties().get(value);
    }
}
