package configuration.models;

import annotation.Value;
import configuration.ValueInjector;
import lombok.*;

@Getter
public class UrlHosts {
    @Value(value = "numbersApi.url.host")
    private String numbersApiHost;

    public void callAnnotationInjector() {
        ValueInjector.inject(this);
    }
}
