package controller;

import configuration.models.UrlHosts;
import models.dto.SymbolAmountDto;
import models.response.NumberApiResponse;
import service.Impl.SymbolServiceImpl;
import service.Impl.UrlConnectionImpl;
import service.SymbolService;
import service.UrlConnectionService;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Scanner;

public class NumberApiController {
    public void run(String[] args) throws IOException {

        UrlHosts urlHosts = new UrlHosts();
        urlHosts.callAnnotationInjector();

        UrlConnectionService urlConnectionService = new UrlConnectionImpl();

        Long number = writeNumber();

        System.out.println("Обращение по адресу: " + urlHosts.getNumbersApiHost() + number + "/trivia");
        HttpURLConnection connection = urlConnectionService.setUrlConnection(urlHosts.getNumbersApiHost() + number + "/trivia");

        NumberApiResponse numberApiResponse = urlConnectionService.getResponse(connection);
        System.out.println("\nСтатус - " + numberApiResponse.getStatus());
        System.out.println("Полученное сообщение: " + numberApiResponse.getMsg());

        SymbolService symbolService = new SymbolServiceImpl();

        System.out.println("\nПодсчет символов: ");
        List<SymbolAmountDto> symbolAmountDtos = symbolService.countSymbols(numberApiResponse.getMsg());
        symbolAmountDtos.forEach(dto -> System.out.println(dto.getSymbol() + " -> " + dto.getAmount()));

        System.out.println("Приближенная частота: ");
        List<String> frequency = symbolService.approximateFrequency((long) numberApiResponse.getMsg().length(), (long) symbolAmountDtos.size(),
                symbolAmountDtos);
        frequency.forEach(f -> System.out.print(f + " "));
    }

    private Long writeNumber() {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите число: ");

        return checkNumber(in.nextLine());
    }

    private Long checkNumber(String arg) {
        long number;

        try {
            number = Long.parseLong(arg);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Введите целое число без точек, запятых и символов.");
        }

        return number;
    }
}
