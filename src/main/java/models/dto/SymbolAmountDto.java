package models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SymbolAmountDto {
    private String symbol;
    private Long amount;
}
