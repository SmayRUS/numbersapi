package models.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NumberApiResponse {
    private int status;
    private String msg;
}
