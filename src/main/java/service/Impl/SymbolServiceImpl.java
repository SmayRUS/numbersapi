package service.Impl;

import models.dto.SymbolAmountDto;
import service.SymbolService;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SymbolServiceImpl implements SymbolService {
    @Override
    public List<SymbolAmountDto> countSymbols(String msg) {
        List<SymbolAmountDto> symbolAmountDtoList = new ArrayList<>();

        msg.replaceAll("[^A-Za-zА-Яа-я0-9]", "").chars().mapToObj( ch -> (char) ch)
            .collect( Collectors.groupingBy( Function.identity(), Collectors.counting() ) )
            .entrySet().stream()
            .forEach(entry -> symbolAmountDtoList.add(new SymbolAmountDto(entry.getKey().toString(), entry.getValue())));

        return symbolAmountDtoList;
    }

    @Override
    public List<String> approximateFrequency(Long amountAll, Long amountKey, List<SymbolAmountDto> symbolAmountDtos) {
        Double average = (double) amountAll / amountKey;

        Long number = Math.round(average);
        System.out.println(amountAll + "/" + amountKey + " = " + average);

        AtomicReference<Long> less = new AtomicReference<>(symbolAmountDtos.get(0).getAmount());

        List<String> frequency = new ArrayList<>();
        symbolAmountDtos.forEach(dto -> {
            if ((number >= dto.getAmount()) & (less.get() <= dto.getAmount())) {
                less.set(dto.getAmount());
                frequency.add(dto.getSymbol() + "(" + Arrays.toString(dto.getSymbol().getBytes(StandardCharsets.UTF_8)) + ")");
            }
        });

        return frequency;
    }
}
