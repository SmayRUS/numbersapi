package service.Impl;

import models.response.NumberApiResponse;
import service.UrlConnectionService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class UrlConnectionImpl implements UrlConnectionService {
    @Override
    public HttpURLConnection setUrlConnection(String host) throws IOException {

        URL url = new URL(host);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        return connection;
    }

    @Override
    public NumberApiResponse getResponse(HttpURLConnection connection) throws IOException {
        int status = connection.getResponseCode();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuffer content = new StringBuffer();
        bufferedReader.lines().forEach(content::append);
        bufferedReader.close();

        connection.disconnect();

        return new NumberApiResponse(status, content.toString());
    }
}
