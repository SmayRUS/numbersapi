package service;

import models.dto.SymbolAmountDto;

import java.util.List;

public interface SymbolService{
    List<SymbolAmountDto> countSymbols(String msg);
    List<String> approximateFrequency(Long amountAll, Long amountKey, List<SymbolAmountDto> symbolAmountDtos);
}
