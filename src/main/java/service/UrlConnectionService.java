package service;

import models.response.NumberApiResponse;

import java.io.IOException;
import java.net.HttpURLConnection;

public interface UrlConnectionService {
    HttpURLConnection setUrlConnection(String host) throws IOException;
    NumberApiResponse getResponse(HttpURLConnection connection) throws IOException;
}
